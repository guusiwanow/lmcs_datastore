var mysql = require("mysql");

function PRODUCT_ROUTER(router,connection,md5) {
    var self = this;
    self.handleRoutes(router,connection,md5);
}

PRODUCT_ROUTER.prototype.handleRoutes= function(router,connection,md5) {

    router.get("/product/", function(req, res){
      var query = "SELECT * FROM ??";
      var table = ["product"];
      query = mysql.format(query, table);
      connection.query(query, function(err, rows) {
        if(err) {
          res.json({"error" : true, "message" : "error execution MySQL query"});
        } else {
          res.json({"error" : false, "Message" : "success", "data" : rows});
        }
      });
    });

    router.post("/product/", function(req, res){
        var query = "INSERT INTO ?? (??,??) VALUES(?,?);";
        var table = ["product", "name","description", req.body.name, req.body.description];
        query = mysql.format(query, table);
        connection.query(query, function(err, rows) {
          if(err) {
            res.json({"error" : true, "message" : "error execution MySQL query"});
          } else {
            res.json({"error" : false, "Message" : "success"});
          }
        });
    });

    router.put("/product/:product_id", function(req, res){
        var query = "UPDATE ?? SET ?? = ?, ?? = ? WHERE ?? = ?";
        var table = ["product", "name", req.body.name, "description", req.body.description, "id", req.params.product_id];
        query = mysql.format(query, table);
        connection.query(query, function(err, rows) {
          if(err) {

            res.json({"error" : true, "message" : "error execution MySQL query"});
          } else {

            res.json({"error" : false, "Message" : "success"});
          }
        });
    });

    router.delete("/product/:product_id", function(req, res){
        var query = "DELETE FROM `product` WHERE id = ?";
        var table = [req.params.product_id];
        query = mysql.format(query, table);
        connection.query(query, function(err, rows) {
          if(err) {

            res.json({"error" : true, "message" : "error execution MySQL query"});
          } else {

            res.json({"error" : false, "Message" : "success"});
          }
        });
    });

}

module.exports = PRODUCT_ROUTER;
