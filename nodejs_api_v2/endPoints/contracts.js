var mysql = require("mysql");

function CONTRACT_ROUTER(router,connection,md5) {
    var self = this;
    self.handleRoutes(router,connection,md5);
}

CONTRACT_ROUTER.prototype.handleRoutes= function(router,connection,md5) {

    router.get("/contract/last_month", function(req, res){
      var query = "SELECT con.id , Date_Format(starting_date,'%d/%m/%Y') AS starting_date, Date_Format(expiration_date,'%d/%m/%Y') AS expiration_date, product_count, total_price, is_paid, com.name FROM contract AS con LEFT JOIN company AS com ON con.company_id = com.id WHERE expiration_date < NOW() + INTERVAL 21 DAY AND expiration_date >= NOW()";
      var table = [];
      query = mysql.format(query, table);
      connection.query(query, function(err, rows) {
        if(err) {
          res.json({"error" : true, "message for first choice" : "error execution MySQL query"});
        } else {
          res.json({"error" : false, "Message for first choice" : "success", "data" : rows});
        }
      });
    });

    router.get("/contract/", function(req, res){
        var query = "SELECT con.id , Date_Format(starting_date,'%d/%m/%Y') AS starting_date, Date_Format(expiration_date,'%d/%m/%Y') AS expiration_date, product_count, total_price, is_paid, com.name, pro.name, pro.description, pro.description FROM contract AS con LEFT JOIN company AS com ON con.company_id = com.id LEFT JOIN product AS pro ON con.product_id = pro.id";
        var table = ["contract"];
        query = mysql.format(query, table);
        connection.query(query, function(err, rows) {
          if(err) {
            res.json({"error" : true, "message for second choice" : "error execution MySQL query"});
          } else {
            res.json({"error" : false, "Message for second choice" : "success", "data" : rows});
          }
        });

    });

    router.post("/contract/", function(req, res){
        var query = "INSERT INTO `contract` ( `starting_date`, `expiration_date`, `product_count`, `total_price`, `is_paid`, `company_id`, `product_id`) VALUES (?, ?, ?, ?, ?, ?, ?)";
        var table = [req.body.starting_date, req.body.expiration_date, req.body.product_count, req.body.total_price, req.body.is_paid, req.body.company_id, req.body.product_id];
        query = mysql.format(query, table);
        connection.query(query, function(err, rows) {
          if(err) {
            res.json({"error" : true, "message" : "error execution MySQL query"});
          } else {
            res.json({"error" : false, "Message" : "success"});
          }
        });
    });

    // router.put("/product/:product_id", function(req, res){
    //     var query = "UPDATE `contract` SET `starting_date` = ?, `expiration_date` = ?, `product_count` = ?, `total_price` = ?, `is_paid` = ?, `company_id` = ?, `product_id` = ? WHERE `id` = ?";
    //     var table = [req.body.starting_date, req.body.expiration_date, req.body.product_count, req.body.total_price, req.body.is_paid, req.body.company_id, req.body.product_id, req.body.product_id];
    //     query = mysql.format(query, table);
    //     connection.query(query, function(err, rows) {
    //       if(err) {
    //         res.json({"error" : true, "message" : "error execution MySQL query"});
    //       } else {
    //         res.json({"error" : false, "Message" : "success"});
    //       }
    //     });
    // });
}

module.exports = CONTRACT_ROUTER;
