var mysql = require("mysql");

function COMPANY_ROUTER(router,connection,md5) {
    var self = this;
    self.handleRoutes(router,connection,md5);
}

COMPANY_ROUTER.prototype.handleRoutes= function(router,connection,md5) {

    router.get("/company/", function(req, res){
      var query = "SELECT * FROM ??";
      var table = ["company"];
      query = mysql.format(query, table);
      connection.query(query, function(err, rows) {
        if(err) {
          res.json({"error" : true, "message" : "error execution MySQL query"});
        } else {
          res.json({"error" : false, "Message" : "success", "data" : rows});
        }
      });
    });

    router.post("/company/", function(req, res){
        var query = "INSERT INTO `company` (`name`) VALUES (?)";
        var table = [req.body.name];
        query = mysql.format(query, table);
        connection.query(query, function(err, rows) {
          if(err) {
            res.json({"error" : true, "message" : "error execution MySQL query"});
          } else {
            res.json({"error" : false, "Message" : "success"});
          }
        });
    });

    router.delete("/company/:company_id", function(req, res){
        console.log("Delete Company");
        var query = "DELETE FROM `company` WHERE id = ?";
        var table = [req.params.company_id];
        query = mysql.format(query, table);
        connection.query(query, function(err, rows) {
          if(err) {

            res.json({"error" : true, "message" : "error execution MySQL query"});
          } else {

            res.json({"error" : false, "Message" : "success"});
          }
        });
    });

    router.put("/company/:company_id", function(req, res){
      console.log("1 update company >", req.body.name, "<", req.params.company_id);
        var query = "UPDATE `company` SET `name` = ? WHERE id = ?";
        var table = [req.body.name, req.params.company_id];
        query = mysql.format(query, table);
        console.log("2 query: ", query);
        connection.query(query, function(err, rows) {
          if(err) {
            console.log("3 Error has occured");
            res.json({"error" : true, "message" : "error execution MySQL query"});
          } else {
            console.log("3 Everything was ok");
            res.json({"error" : false, "Message" : "success"});
          }
        });
    });
}

module.exports = COMPANY_ROUTER;
