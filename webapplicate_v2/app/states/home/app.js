app.controller("homeController", function($scope, $state, company_http, contract_http, product_http){

  var ctrl = this;
  this.title = $state.current.title;

  this.contracts = [];
  this.companys = [];
  this.products = [];

  this.getAllCompanys = function() {
    company_http.getAllCompanys().get(function(response) {
      ctrl.companys = response.data;
    });
  }

  this.getAllContracts = function() {
    contract_http.getContractDataFromLastMonth().get(function(response){
      console.log(response);
      ctrl.contracts = response.data;
    });
  };

  this.getAllProducts = function() {
    product_http.getAllProducts().get(function(response){
      ctrl.products = response.data;
    });
  };

  this.getAllContracts();
  this.getAllCompanys();
  this.getAllProducts();
});
