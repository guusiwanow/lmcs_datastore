app.controller("companyController", function($scope, $state, company_http){

  var ctrl = this;
  this.title = $state.current.title;
  this.companys = [];

  this.editMode = false;

  this.tempCompany = {};
  this.addNewMode = false;


  this.getAllCompanys = function() {
    company_http.getAllCompanys().get(function(response){
      console.log("company" , response.data);
      ctrl.companys = response.data;
    });
  };

  this.save = function(company) {
    company_http.updateCompany(company); //.update()
    ctrl.toggleEdit(company);
  };

  this.delete = function(company) {
    company_http.deleteCompany(company).delete(function(response) {
      ctrl.getAllCompanys();
    });
  }

  this.toggleAddNew = function() {
    if(ctrl.addNewMode != true) {
      ctrl.addNewMode = true;
    } else {
      ctrl.addNewMode = false;
    }
  }

  this.saveNewCompany = function() {
    company_http.addNewCompany().create(ctrl.tempCompany, function(response) {
      ctrl.getAllCompanys();
    });
    ctrl.toggleAddNew();
    ctrl.tempCompany = {};

  }

  this.toggleEdit = function(company) {
    if(company.editMode != true) {
      company.editMode = true;
    } else {
      company.editMode = false;
    }
    console.log(ctrl.companys);
  };

  this.getAllCompanys();
});
