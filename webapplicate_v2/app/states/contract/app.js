app.controller("contractController", function($scope, $state, company_http, contract_http, product_http){

  var ctrl = this;
  this.title = $state.current.title;
  this.contracts = [];
  this.companys = [];
  this.products = [];

  this.editMode = false;

  this.tempContract = {};
  this.addNewMode = false;

  this.getAllCompanys = function() {
    company_http.getAllCompanys().get(function(response) {
      ctrl.companys = response.data;
    });
  }

  this.getAllContracts = function() {
    contract_http.getContractData().get(function(response){
      console.log("data 123", response.data);
      ctrl.contracts = response.data;
    });
  };

  this.getAllProducts = function() {
    product_http.getAllProducts().get(function(response){
      ctrl.products = response.data;
    });
  }

  this.addNewContract = function(newContract) {
    console.log(newContract);
    // http_controller.addNewContract().create(newContract, function(response) {
    //   ctrl.getAllContracts();
    // });
    // ctrl.toggleAddNew();
    // ctrl.tempProduct = {};
  }

  this.save = function(contract) {
    ctrl.toggleEdit(contract);
  };

  this.toggleAddNew = function() {
    if(ctrl.addNewMode != true) {
      ctrl.addNewMode = true;
    } else {
      ctrl.addNewMode = false;
    }
  }

  this.toggleEdit = function(contract) {
    if(contract.editMode != true) {
      contract.editMode = true;
    } else {
      contract.editMode = false;
    }
    console.log(ctrl.contract);
  };

  this.getAllContracts();
  this.getAllCompanys();
  this.getAllProducts();
});
