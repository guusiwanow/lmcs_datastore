app.controller("productController", function($scope, $state, product_http){

  var ctrl = this;
  this.title = $state.current.title;
  this.products = [];

  this.editMode = false;

  this.tempProduct = {};
  this.addNewMode = false;


  this.getAllProducts = function() {
    product_http.getAllProducts().get(function(response){
      console.log("products" , response.data);
      ctrl.products = response.data;
    });
  };

  this.save = function(product) {
    product_http.updateProduct(product); //.update()
    ctrl.toggleEdit(product);
  };

  this.toggleAddNew = function() {
    if(ctrl.addNewMode != true) {
      ctrl.addNewMode = true;
    } else {
      ctrl.addNewMode = false;
    }
  }

  this.saveNewProduct = function() {
    product_http.addNewProduct().create(ctrl.tempProduct, function(response) {
      ctrl.getAllProducts();
    });
    ctrl.toggleAddNew();
    ctrl.tempProduct = {};

  }

  this.delete = function(product) {
    product_http.deleteProduct(product).delete(function(response) {
      ctrl.getAllProducts();
    });
  }

  this.toggleEdit = function(product) {
    if(product.editMode != true) {
      product.editMode = true;
    } else {
      product.editMode = false;
    }
    console.log(ctrl.products);
  };

  this.getAllProducts();
});
