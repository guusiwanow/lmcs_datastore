"use strict";
angular.module("myApp").service("company_http", function($resource, $http){

  var api_url = 'http://localhost:3000/api';

  this.getAllCompanys = function() {
    return $resource(api_url + '/company/');
  }

  this.addNewCompany = function() {
    return $resource('http://localhost:3000/api/company/', null,
        {
            'create': { method:'POST'}
          });
  };

  this.updateCompany = function(company) {
    console.log("Update company");
    $resource('http://localhost:3000/api/company/'+company.id, company,
        {
            'update': { method:'PUT'}
        }).update(company);
  };

  this.deleteCompany = function(company) {
    console.log("Delete company");
    // $http.delete('http://localhost:3000/api/company/'+company.id);
    return $resource('http://localhost:3000/api/company/' + company.id ,
        {
            'delete': { method:'DELETE'}
        });
  };

});
