"use strict";
angular.module("myApp").service("product_http", function($resource){

  var api_url = 'http://localhost:3000/api';

  this.getAllProducts = function() {
    return $resource(api_url + '/product/');
  };

  this.addNewProduct = function() {
    return $resource('http://localhost:3000/api/product/', null,
        {
            'create': { method:'POST'}
          });
  };

  this.updateProduct = function(product) {
    console.log("abc");
    $resource('http://localhost:3000/api/product/'+product.id, product,
        {
            'update': { method:'PUT'}
        }).update(product);
  };

  this.deleteProduct = function(product) {
    console.log("Delete product");
    // $http.delete('http://localhost:3000/api/company/'+company.id);
    return $resource('http://localhost:3000/api/product/' + product.id ,
        {
            'delete': { method:'DELETE'}
        });
  };

});
