"use strict";
angular.module("myApp").service("contract_http", function($resource){

  this.getContractDataFromLastMonth = function() {
    return $resource('http://localhost:3000/api/contract/last_month');
  };

  // this.getContractData = function() {
  //   return $resource('http://localhost:3000/api/contract/', null,
  //       {
  //           'create': { method:'POST'}
  //       });
  // };


  var api_url = 'http://localhost:3000/api';

  this.getAllContracts = function() {
    return $resource(api_url + '/contract/', null, {
      'create': {method : 'POST'}
    });
  };

  this.addNewContract = function() {
    return $resource('http://localhost:3000/api/contract/', null,
        {
            'create': { method:'POST'}
          });
  };

  this.updateContract = function(contract) {
    console.log("Update contract");
    $resource('http://localhost:3000/api/contract/'+contract.id, contract,
        {
            'update': { method:'PUT'}
        }).update(contract);
  };

  this.deleteContract = function(contract) {
    console.log("Delete contract");
    return $resource('http://localhost:3000/api/contract/' + contract.id ,
        {
            'delete': { method:'DELETE'}
        });
  };

});
