app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider

    .state('contract', {
        url: '/contract',
        templateUrl: '/states/contract/index.html',
        title: "Contracten pagina",
        controller: "contractController",
        controllerAs: "ctrl"
    }).state('company', {
        url: '/company',
        templateUrl: '/states/company/index.html',
        title: "Company pagina",
        controller: "companyController",
        controllerAs: "ctrl"
    }).state('home', {
        url: '/home',
        templateUrl: '/states/home/index.html',
        title: "home pagina",
        controller: "homeController",
        controllerAs: "ctrl"
    }).state('product', {
        url: '/product',
        templateUrl: '/states/product/index.html',
        title: "Producten pagina",
        controller: "productController",
        controllerAs: "ctrl"
    });

});
