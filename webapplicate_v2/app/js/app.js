app.controller("mainController", function($state, $scope, $mdSidenav, $mdComponentRegistry){

    var ctrl = this;

    this.toggle = angular.noop;

    this.title = $state.current.title;

    this.isOpen = function() { return false };

    $mdComponentRegistry
    .when("left")
    .then( function(sideNav){
      ctrl.isOpen = angular.bind( sideNav, sideNav.isOpen );
      ctrl.toggle = angular.bind( sideNav, sideNav.toggle );
    });

    this.toggleRight = function() {
    console.log("TESTING");
    $mdSidenav("left").toggle()
        .then(function(){
        });
    };

    this.close = function() {
    console.log("TESTING");
    $mdSidenav("right").close()
        .then(function(){
        });
    };
});
