var mysql = require("mysql");

function REST_ROUTER(router,connection,md5) {
    var self = this;
    self.handleRoutes(router,connection,md5);
}

REST_ROUTER.prototype.handleRoutes= function(router,connection,md5) {
    router.get("/",function(req,res){
      res.json({"Message" : "This is the api LMCS_Databank_Api !"});
    });

    // PRODUCTS END POINTS
    router.get("/product/", function(req, res){
      var query = "SELECT * FROM ??";
      var table = ["product"];
      query = mysql.format(query, table);
      connection.query(query, function(err, rows) {
        if(err) {
          res.json({"error" : true, "message" : "error execution MySQL query"});
        } else {
          res.json({"error" : false, "Message" : "success", "data" : rows});
        }
      });
    });

    router.get("/product/:product_id", function(req, res){
      var query = "SELECT * FROM ?? as ?? WHERE ?? = ?";
      var table = ["product", "p", "p.id", req.params.product_id];
      query = mysql.format(query, table);
      connection.query(query, function(err, rows) {
        if(err) {
          res.json({"error" : true, "message" : "error execution MySQL query"});
        } else {
          res.json({"error" : false, "Message" : "success", "data" : rows});
        }
      });
    });

    router.get("/product/:product_id/company", function(req, res){
      //SELECT id, name FROM company_has_product as chp LEFT JOIN company AS c ON chp.Company_id = c.id WHERE Product_id = 2;
      var query = "SELECT ?? , ?? FROM ?? AS ?? LEFT JOIN ?? AS ?? ON ?? = ?? WHERE ?? = ?";
      var table = ["id","name","company_has_product", "chp", "company", "c", "chp.Company_id", "c.id","Product_id", req.params.product_id];
      query = mysql.format(query, table);
      connection.query(query, function(err, rows) {
        if(err) {
          res.json({"error" : true, "message" : "error execution MySQL query", "query" : query});
        } else {
          res.json({"error" : false, "Message" : "success", "data" : rows});
        }
      });
    });

    router.get("/product/:product_id/license", function(req, res){
      var query = "SELECT * FROM ?? as ?? WHERE ?? = ?";
      var table = ["licence", "l", "l.Product_id", req.params.product_id];
      query = mysql.format(query, table);
      connection.query(query, function(err, rows) {
        if(err) {
          res.json({"error" : true, "message" : "error execution MySQL query"});
        } else {
          res.json({"error" : false, "Message" : "success", "data" : rows});
        }
      });
    });

    // COMPANY END POINTS
    router.get("/company/", function(req, res){
      var query = "SELECT * FROM ??";
      var table = ["company"];
      query = mysql.format(query, table);
      connection.query(query, function(err, rows) {
        if(err) {
          res.json({"error" : true, "message" : "error execution MySQL query"});
        } else {
          res.json({"error" : false, "Message" : "success", "data" : rows});
        }
      });
    });

    router.get("/company/:company_id", function(req, res){
      var query = "SELECT * FROM ?? AS ?? WHERE ?? = ?";
      var table = ["company", "c", "c.id", req.params.company_id];
      query = mysql.format(query, table);
      connection.query(query, function(err, rows) {
        if(err) {
          res.json({"error" : true, "message" : "error execution MySQL query"});
        } else {
          res.json({"error" : false, "Message" : "success", "data" : rows});
        }
      });
    });

    router.get("/company/:company_id/product", function(req, res){
      //SELECT id, name FROM company_has_product as chp LEFT JOIN company AS c ON chp.Company_id = c.id WHERE Product_id = 2;
      //SELECT id, name FROM company_has_product as chp LEFT JOIN product AS p ON chp.Product_id = p.id WHERE Company_id = 2;
      var query = "SELECT id, name, date_format(expiration_date, '%d-%m-%Y') AS expiration_date FROM ?? AS ?? LEFT JOIN ?? AS ?? ON ?? = ?? WHERE ?? = ? ORDER BY expiration_date DESC";
      var table = ["company_has_product", "chp", "product", "p", "chp.Product_id", "p.id","Company_id", req.params.company_id];
      query = mysql.format(query, table);
      connection.query(query, function(err, rows) {
        if(err) {
          res.json({"error" : true, "message" : "error execution MySQL query", "query" : query});
        } else {
          res.json({"error" : false, "Message" : "success", "data" : rows, "query" : query});
        }
      });
    });
}

module.exports = REST_ROUTER;
