-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2016 at 08:00 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lmcs_datastore`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`) VALUES
(1, 'LMCS'),
(2, 'Verbeco');

-- --------------------------------------------------------

--
-- Table structure for table `company_has_product`
--

CREATE TABLE `company_has_product` (
  `Company_id` int(11) NOT NULL,
  `Product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_has_product`
--

INSERT INTO `company_has_product` (`Company_id`, `Product_id`) VALUES
(1, 1),
(1, 2),
(2, 2),
(2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `licence`
--

CREATE TABLE `licence` (
  `id` int(11) NOT NULL,
  `licence_key` varchar(512) DEFAULT NULL,
  `Product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `licence`
--

INSERT INTO `licence` (`id`, `licence_key`, `Product_id`) VALUES
(1, 'TESTPDOCUTDKEY', 3),
(2, 'TESTPDOCUTDKEY', 3);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `online_backup` tinyint(1) DEFAULT NULL,
  `disk_space` varchar(45) DEFAULT NULL,
  `account_count` int(11) DEFAULT NULL,
  `starting_date` date DEFAULT NULL,
  `expiration_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='									';

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `description`, `online_backup`, `disk_space`, `account_count`, `starting_date`, `expiration_date`) VALUES
(1, 'theName', 'TheDescription', 0, '250GB', 9, '2012-08-12', '2014-09-14'),
(2, 'theName', 'TheDescription', 0, '250GB', 9, '2012-08-12', '2014-09-14'),
(3, 'theName', 'TheDescription', 0, '250GB', 9, '2012-08-12', '2014-09-14'),
(4, 'theName', 'TheDescription', 0, '250GB', 9, '2012-08-12', '2014-09-14'),
(5, 'theName', 'TheDescription', 0, '250GB', 9, '2012-08-12', '2014-09-14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_has_product`
--
ALTER TABLE `company_has_product`
  ADD PRIMARY KEY (`Company_id`,`Product_id`),
  ADD KEY `fk_Company_has_Product_Product1_idx` (`Product_id`),
  ADD KEY `fk_Company_has_Product_Company_idx` (`Company_id`);

--
-- Indexes for table `licence`
--
ALTER TABLE `licence`
  ADD PRIMARY KEY (`id`,`Product_id`),
  ADD KEY `fk_Licence_Product1_idx` (`Product_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `licence`
--
ALTER TABLE `licence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `company_has_product`
--
ALTER TABLE `company_has_product`
  ADD CONSTRAINT `fk_Company_has_Product_Company` FOREIGN KEY (`Company_id`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Company_has_Product_Product1` FOREIGN KEY (`Product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `licence`
--
ALTER TABLE `licence`
  ADD CONSTRAINT `fk_Licence_Product1` FOREIGN KEY (`Product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
